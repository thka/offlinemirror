#!/usr/bin/python3

# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

import sys
import os
import re
import argparse
import hashlib       # Calculate hash of downloaded files
import requests
import json          # Cache file uses json as backend database format
import urllib.parse
import shutil        # Copy a downloaded file to new location
import toml          # Read configuration/settings
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from typing import Tuple, List, Dict, Any, Union, MutableMapping
from debianformat.releasefile import Releasefile, Releasefileinfo
from debianformat.packagesfile import Packagesfile, Packageinfo
from debianformat.sha256sums import Sha256sums

import logging
log = logging.getLogger('offlinemirror')
# How to log on console, when run interactive
try:
    from systemd import journal
    log.setLevel(logging.INFO)
    if 'INVOCATION_ID' in os.environ:
        log.addHandler(journal.JournalHandler(SYSLOG_IDENTIFIER='offlinemirror'))
    else:
        handler = logging.StreamHandler(stream=sys.stdout)
        formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y.%m.%d %H:%M:%S")
        handler.setFormatter(formatter)
        log.addHandler(handler)
except ImportError:
    handler = logging.StreamHandler(stream=sys.stdout)
    if 'INVOCATION_ID' not in os.environ:
        formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y.%m.%d %H:%M:%S")
        handler.setFormatter(formatter)

    log.addHandler(handler)

# class localcacheobject(TypedDict):
#     filename: Required[str]
#     size: Required[int]
#     lastupdate: Required[str]
#     md5: NotRequired[str]
#     sha1: NotRequired[str]
#     sha256: NotRequired[str]
#     sha384: NotRequired[str]
#     sha512: NotRequired[str]

proxies: Dict[str, str] = dict()
verify: Union[bool, str] = True


class Cachedatabase:
    def __init__(self, filename: str, expire_days: int = 30):
        """
        Initialises a cache database
        Arguments:
            filename: The cache filename
            expire_days: Maximum days a cache entry shall live
        """
        self.cachefilename = filename
        self.cache: Dict[str, Dict[str, Union[str, int, bool]]] = dict()
        self.expire_days = expire_days
        self.unsaved_entries = 0
        self.autosave_every = 0

    def save(self, remove_expired_entries: bool = False) -> int:
        """
            Saves the cache and removes old entries
            Returns number of entries
        """
        if not os.path.exists(os.path.dirname(self.cachefilename)):
            os.makedirs(os.path.dirname(self.cachefilename))
        with open(self.cachefilename, 'w') as cachefile:
            tempdict = dict()
            for onefile in self.files():
                if remove_expired_entries:
                    if self.days_old(onefile) <= self.expire_days:
                        tempdict[onefile] = self.cache_entry(onefile)
                else:
                    tempdict[onefile] = self.cache_entry(onefile)

            cachefile.write(json.dumps(tempdict, indent=4))
            self.unsaved_entries = 0

            return len(self.cache)

        return 0

    def load(self) -> int:
        """
            Loads a cache from disk
            Removes expired entries on load
            Returns number of loaded entries
        """
        if os.path.exists(self.cachefilename):
            with open(self.cachefilename, 'rt') as cachefile:
                try:
                    self.cache = json.loads(cachefile.read())
                except json.decoder.JSONDecodeError:
                    self.cache = dict()
                return len(self.cache)

        for onefile in self.files():
            if self.expired(onefile):
                del self.cache[onefile]

        return 0

    def add(self, cacheobject: str, cachesize: int = -1,
            hashstring: str = '', negative_cache: bool = False) -> None:
        """
            Add a file to the cache
            If SIZE is None, then the file is checked for size
            If HASH is None, then it will be calculated
        """
        localobject: Dict[str, Union[str, int, bool]] = dict()
        filesize = cachesize
        filehash = hashstring
        localobject['filename'] = cacheobject
        localobject['negative_cache'] = negative_cache
        if cachesize == -1:
            if os.path.exists(cacheobject):
                fileinfo = os.stat(cacheobject)
                filesize = fileinfo.st_size
        localobject['size'] = filesize

        if hashstring == '':
            if os.path.exists(cacheobject):
                filehash = self.calcdigest(cacheobject)
            else:
                filehash = str()
        localobject[self.digesttype(filehash)] = filehash

        localobject['lastseen'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.cache[cacheobject] = localobject

        self.unsaved_entries += 1
        if self.autosave_every > 0 and self.unsaved_entries >= self.autosave_every:
            _ = self.save()

        return None

    def remove(self, cacheobject: str) -> bool:
        if cacheobject not in self.cache:
            return False

        del self.cache[cacheobject]

        return True

    def touch(self, cacheobject: str) -> str:
        if cacheobject not in self.cache:
            return str()

        self.cache[cacheobject]['lastseen'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        return str(self.cache_entry(cacheobject)['lastseen'])

    def days_old(self, cacheobject: str) -> int:
        if cacheobject not in self.cache:
            return 0
        if 'lastseen' not in self.cache[cacheobject]:
            return 0

        parsed_date = re.fullmatch(r'(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})',
                                   str(self.cache[cacheobject]['lastseen']))
        lastupdate = datetime(int(parsed_date.group(1)), int(parsed_date.group(2)), int(parsed_date.group(3)),
                              int(parsed_date.group(4)), int(parsed_date.group(5)), int(parsed_date.group(6)))
        timenow = datetime.now()

        return int((timenow - lastupdate) / timedelta(days=1))

    def files(self) -> List[str]:
        return list(self.cache.keys())

    def cache_entry(self, cacheobject: str) -> Dict[str, Union[str, int]]:
        if cacheobject not in self.cache:
            return dict()

        return self.cache[cacheobject]

    def exists(self, cacheobject: str, addfile: bool = False) -> bool:
        """
            If file is missing in cache,
            then optional check filesystem and add it
            Arguments:
                cacheobject: The key (filename)
                addfile: if the file automatically will be added if
                        non-existent in cache and existent in filesystem
        """
        if cacheobject in self.cache:
            return True

        if os.path.exists(cacheobject) and addfile:
            fileinfo = os.stat(cacheobject)
            self.add(cacheobject, fileinfo.st_size,
                     self.calcdigest(cacheobject, 'sha256'))

            return True

        return False

    def calcdigest(self, cacheobject: str, digesttype: str = 'sha256') -> str:
        """
            Calculate the digest of the file/cacheobject

            Arguments:
                cacheobject: The key (filename)
                digesttype: name of supported type from hashlib, default is
                            SHA256
        """
        if os.path.exists(cacheobject):
            with open(cacheobject, 'rb') as checkfile:
                digest = hashlib.new(digesttype)
                digest.update(checkfile.read())

                return str(digest.hexdigest())

        return str()

    def comparedigest(self, cacheobject: str, digest: str) -> bool:
        """
            Compare a cached object's digest with the input

            Arguments:
                cacheobject: The key (filename)
                digest: The digest to compare with
        """
        if self.digest(cacheobject, self.digesttype(digest)) == digest:
            return True

        return False

    def setdigest(self, cacheobject: str, digest: str):
        """
            Set a digest to an object

            Arguments:
                cacheobject: The key (filename)
                digest: The digest to set on objwect

        """
        if cacheobject not in self.cache:
            return None

        self.cache[cacheobject][self.digesttype(digest)] = digest

        return None

    def size(self, cacheobject: str) -> int:
        """
            Get file size on an object

            Arguments:
                cacheobject: The key (filename)

        """
        if cacheobject not in self.cache:
            return -1

        if 'size' not in self.cache_entry(cacheobject):
            return -1

        return int(self.cache_entry(cacheobject)['size'])

    def lastseen(self, cacheobject: str) -> str:
        """
            Get last seen date on an object

            Arguments:
                cacheobject: The key (filename)

        """
        if cacheobject not in self.cache:
            return str()

        if 'lastseen' not in self.cache_entry(cacheobject):
            return str()

        return str(self.cache_entry(cacheobject)['lastseen'])

    def digest(self, cacheobject: str, digesttype: str = 'sha256') -> str:
        """
            Get a digest on an object

            Arguments:
                cacheobject: The key (filename)
                digest: Return this digest, if it exists

        """
        if cacheobject not in self.cache:
            return str()

        if digesttype in self.cache[cacheobject]:
            return str(self.cache[cacheobject][digesttype])

        return str()

    def digesttype(self, digestsum: str) -> str:
        """
            Returns the digest type of the sum

            Arguments:
                digestsum: The sum to analyse
        """
        if len(digestsum) == 32:
            return 'md5'
        elif len(digestsum) == 40:
            return 'sha1'
        elif len(digestsum) == 64:
            return 'sha256'
        elif len(digestsum) == 96:
            return 'sha384'
        elif len(digestsum) == 128:
            return 'sha512'
        else:
            return 'unknown'

    def expired(self, cacheobject: str) -> bool:
        """
            If max cache time is true, then it's expired

            Returns:
                True/Expired if cache object does not exist
                True/Expired if cache object is older than max time
        """

        if not self.exists(cacheobject):
            return True

        if self.days_old(cacheobject) < self.expire_days:
            return False

        return True

    def validate(self, days: int = 30, hashcheck: bool = False) -> int:
        """
            Goes through every cached entry and check for
                - existance
                - size test
                - optional digest test

            Arguments:
                days: Max number of days before a cache entry is expired
                hashcheck: Calculate digest from file in filesystem
        """
        deleted = 0
        delete_candidate = list()
        for onefile in self.files():
            if not os.path.exists(onefile):
                delete_candidate.append(onefile)
                deleted += 1
                continue
            if days > 0 and self.days_old(onefile) > days:
                delete_candidate.append(onefile)
                deleted += 1
                continue
            fileinfo = os.stat(onefile)
            if fileinfo.st_size != self.cache_entry(onefile)['size']:
                delete_candidate.append(onefile)
                deleted += 1
                continue
            if hashcheck:
                with open(onefile, 'rb') as checkfile:
                    digest = hashlib.new('sha256')
                    digest.update(checkfile.read())
                    if digest.hexdigest() != self.cache_entry(onefile)['sha256']:
                        delete_candidate.append(onefile)
                        deleted += 1
                        continue

        for one in delete_candidate:
            del self.cache[one]

        return deleted

    def already_cached(self, filename: str, digest: str = str()) -> bool:
        if self.expired(filename):
            _ = self.remove(filename)
        if self.exists(filename):
            if digest != str() and self.digest(filename) == digest:
                if self.expire_days > 0:
                    return True
            if digest == str():
                if self.expire_days > 0:
                    return True
        if os.path.exists(filename):
            if self.calcdigest(filename) == digest:
                self.add(filename)
                return True

        return False

    def negative_cached(self, filename: str) -> bool:
        if not self.exists(filename):
            return False
        entry = self.cache_entry(filename)
        if 'negative_cache' not in entry:
            return False

        return bool(entry['negative_cache'])


class Componentsobject:
    """In ['main', 'contrib']"""
    def __init__(self, comps: List[str]):
        self.comps = comps

    @property
    def list(self) -> List[str]:
        return self.comps

    @list.setter
    def list(self, newsetting: Union[str, List[str]]):
        if isinstance(newsetting, list):
            self.comps = newsetting
        else:
            self.comps = [newsetting]

    def add(self, compsname: str):
        if isinstance(compsname, str):
            if compsname not in self.comps:
                self.comps.append(compsname)

    def remove(self, compsname: str):
        if isinstance(compsname, str):
            self.comps.remove(compsname)

    def path(self, compsname: str) -> str:
        """Returns example 'main' """
        return compsname


class Distributionsobject:
    """In ['bionic', 'focal']"""
    def __init__(self, dists: List[str]):
        self.dists = dists

    @property
    def list(self):
        return self.dists

    @list.setter
    def list(self, newsetting: Union[str, List[str]]):
        if isinstance(newsetting, list):
            self.dists = newsetting
        else:
            self.dists = [newsetting]

    def add(self, distsname: str):
        if isinstance(distsname, str):
            if distsname not in self.dists:
                self.dists.append(distsname)

    def remove(self, distsname: str):
        if isinstance(distsname, str):
            self.dists.remove(distsname)

    def path(self, distsname: str):
        """Returns example 'dists/bionic' """
        return os.path.join("dists", distsname)


class Architecturesobject:
    """In ['amd64', 'i386']"""
    def __init__(self, archs: List[str]):
        self.archs = archs

    @property
    def list(self):
        return self.archs

    @list.setter
    def list(self, newsetting: Union[str, List[str]]):
        if isinstance(newsetting, list):
            self.archs = newsetting
        else:
            self.archs = [newsetting]

    def add(self, archname: str):
        if isinstance(archname, str):
            if archname not in self.archs:
                self.archs.append(archname)

    def remove(self, archname: str):
        if isinstance(archname, str):
            self.archs.remove(archname)


class Repoobject:
    def __init__(self, reponame: str, repodict: Dict[str, Any], http_proxy: str = str(),
                 https_proxy: str = str(), tls_verify: Union[bool, str] = True):
        self.reponame = reponame
        # self.repoconfig: Dict[str, Any] = repodict
        self.repoconfig: Dict[str, Any] = dict()

        self.default_tls_verify = tls_verify
        self.default_http_proxy = http_proxy
        self.default_https_proxy = https_proxy
        self.default_autosave = 500
        self.default_enabled = False
        self.default_method = 'http'
        self.default_format = 'deb'
        self.default_host = ''
        self.default_remote_directory = '/'
        self.default_destination_directory = '.'
        self.default_cache_directory = os.path.join(self.default_destination_directory, 'cache')
        self.default_distributions = Distributionsobject(list())
        self.default_components = Componentsobject(['main'])
        self.default_architectures = Architecturesobject(['amd64'])
        self.default_packagename_regexp = r''
        self.default_filename_regexp = r''
        self.default_download_packages = True
        self.default_download_diffs = True
        self.default_download_by_hash = True
        self.default_create_by_hash = False
        self.default_debian_installer = False
        self.default_command_not_found = False
        self.default_cnf = False
        self.default_dep11 = False
        self.default_dist_upgrader_all = False
        self.default_i18n = False
        self.default_languages: List[str] = list()
        self.default_installer = False
        self.default_uefi = False
        self.default_signed = False
        self.default_source = False

        if 'distributions' in repodict:
            if isinstance(repodict['distributions'], list):
                self.repoconfig['distributions'] = Distributionsobject(repodict['distributions'])

        if 'components' in repodict:
            if isinstance(repodict['components'], list):
                self.repoconfig['components'] = Componentsobject(repodict['components'])

        if 'architectures' in repodict:
            if isinstance(repodict['architectures'], list):
                self.repoconfig['architectures'] = Architecturesobject(repodict['architectures'])
                self.repoconfig['architectures'].add('all')

        for one_key in repodict:
            if one_key not in self.repoconfig:
                self.repoconfig[one_key] = repodict[one_key]

    def extra_tags(self) -> List[str]:
        returntags = list()
        if self.debian_installer:
            returntags.append('debian-installer')
        if self.cnf:
            returntags.append('cnf')
        if self.dep11:
            returntags.append('dep11')
        if self.dist_upgrader_all:
            returntags.append('dist-upgrader-all')
        if self.i18n:
            returntags.append('i18n')
        if self.installer:
            returntags.append('installer')
        if self.uefi:
            returntags.append('uefi')
        if self.signed:
            returntags.append('signed')
        if self.source:
            returntags.append('source')

        return returntags

    @property
    def name(self) -> str:
        """Returns the name of the repo"""

        return self.reponame

    def enable(self):
        """enable repo"""
        self.repoconfig['enabled'] = True

    def disable(self):
        """disable repo"""
        self.repoconfig['enabled'] = False

    @property
    def enabled(self) -> bool:
        """Is the repo enabled"""
        if 'enabled' not in self.repoconfig:
            return self.default_enabled

        return self.repoconfig['enabled']

    @enabled.setter
    def enabled(self, newsetting: bool):
        """Is the repo enabled"""
        self.repoconfig['enabled'] = newsetting

    @property
    def tls_verify(self) -> Union[bool, str]:
        """Return tls verify"""
        if 'tls_verify' not in self.repoconfig:
            return self.default_tls_verify

        return self.repoconfig['tls_verify']

    @tls_verify.setter
    def tls_verify(self, newsetting: Union[bool, str]):
        """Return tls verify"""
        if isinstance(newsetting, bool):
            self.repoconfig['tls_verify'] = newsetting
        if isinstance(newsetting, str):
            self.repoconfig['tls_verify'] = newsetting

    @property
    def http_proxy(self) -> str:
        """Return the http_proxy"""
        if 'http_proxy' not in self.repoconfig:
            return self.default_http_proxy

        return self.repoconfig['http_proxy']

    @http_proxy.setter
    def http_proxy(self, newsetting: str):
        """Set http_proxy"""
        if isinstance(newsetting, str):
            self.repoconfig['http_proxy'] = newsetting

    @property
    def https_proxy(self) -> str:
        """Return the https_proxy"""
        if 'https_proxy' not in self.repoconfig:
            return self.default_https_proxy

        return self.repoconfig['https_proxy']

    @https_proxy.setter
    def https_proxy(self, newsetting: str):
        """Set https_proxy"""
        if isinstance(newsetting, str):
            self.repoconfig['https_proxy'] = newsetting

    @property
    def autosave(self) -> int:
        """Return the autosave"""
        if 'autosave' not in self.repoconfig:
            return int(self.default_autosave)

        return self.repoconfig['autosave']

    @autosave.setter
    def autosave(self, newsetting: int):
        """Set autosave"""
        if isinstance(newsetting, int):
            self.repoconfig['autosave'] = newsetting

    @property
    def method(self) -> str:
        """Return the method"""
        if 'method' not in self.repoconfig:
            return self.default_method

        return self.repoconfig['method']

    @method.setter
    def method(self, newsetting: str):
        """Set method"""
        if isinstance(newsetting, str):
            if newsetting in ['http', 'https']:
                self.repoconfig['method'] = newsetting

    @property
    def format(self) -> str:
        """Return the format"""
        if 'format' not in self.repoconfig:
            return self.default_format

        return self.repoconfig['format']

    @format.setter
    def format(self, newsetting: str):
        """Set format"""
        if isinstance(newsetting, str):
            if newsetting in ['deb', 'flat']:
                self.repoconfig['format'] = newsetting

    @property
    def host(self) -> str:
        """Return the host"""
        if 'host' not in self.repoconfig:
            return self.default_host

        return self.repoconfig['host']

    @host.setter
    def host(self, newsetting: str):
        """Set host"""
        if isinstance(newsetting, str):
            self.repoconfig['host'] = newsetting

    @property
    def remote_directory(self) -> str:
        """Return the remote_directory"""
        if 'remote_directory' not in self.repoconfig:
            return self.default_remote_directory

        return self.repoconfig['remote_directory']

    @remote_directory.setter
    def remote_directory(self, newsetting: str):
        """Set remote_directory"""
        if isinstance(newsetting, str):
            self.repoconfig['remote_directory'] = newsetting

    @property
    def destination_directory(self) -> str:
        """Return the destination_directory"""
        if 'destination_directory' not in self.repoconfig:
            return self.default_destination_directory

        return self.repoconfig['destination_directory']

    @destination_directory.setter
    def destination_directory(self, newsetting: str):
        """Set destination_directory"""
        if isinstance(newsetting, str):
            self.repoconfig['destination_directory'] = newsetting

    @property
    def temp_destination_directory(self) -> str:
        """Return the temp_destination_directory"""
        return os.path.join(self.destination_directory, f'.offlinemirror.{self.name}')

    @property
    def cache_directory(self) -> str:
        """Return the cache_directory"""
        if 'cache_directory' not in self.repoconfig:
            return self.default_cache_directory

        return self.repoconfig['cache_directory']

    @cache_directory.setter
    def cache_directory(self, newsetting: str):
        """Set cache_directory"""
        if isinstance(newsetting, str):
            self.repoconfig['cache_directory'] = newsetting

    @property
    def distributions(self):
        """List of distribution objects
           Return the distributions"""
        if 'distributions' not in self.repoconfig:
            return self.default_distributions

        return self.repoconfig['distributions']

    @property
    def components(self) -> Componentsobject:
        """Return the components"""
        if 'components' not in self.repoconfig:
            return self.default_components

        return self.repoconfig['components']

    @property
    def architectures(self) -> Architecturesobject:
        """Return the architectures"""
        if 'architectures' not in self.repoconfig:
            return self.default_architectures

        return self.repoconfig['architectures']

    @property
    def packagename_regexp(self) -> str:
        """Return the regexp for package name"""
        if 'packagename_regexp' not in self.repoconfig:
            return self.default_packagename_regexp

        return self.repoconfig['packagename_regexp']

    @property
    def filename_regexp(self) -> str:
        """Return the regexp for filename"""
        if 'filename_regexp' not in self.repoconfig:
            return self.default_filename_regexp

        return self.repoconfig['filename_regexp']

    @property
    def download_packages(self) -> bool:
        """Return the download packages setting"""
        if 'download_packages' not in self.repoconfig:
            return self.default_download_packages

        return self.repoconfig['download_packages']

    @download_packages.setter
    def download_packages(self, newsetting: bool):
        """Set download_packages"""
        if isinstance(newsetting, bool):
            self.repoconfig['download_packages'] = newsetting

    @property
    def download_diffs(self) -> bool:
        """Return the download diffs setting"""
        if 'download_diffs' not in self.repoconfig:
            return self.default_download_diffs

        return self.repoconfig['download_diffs']

    @download_diffs.setter
    def download_diffs(self, newsetting: bool):
        """Set download_diffs"""
        if isinstance(newsetting, bool):
            self.repoconfig['download_diffs'] = newsetting

    @property
    def download_by_hash(self) -> bool:
        """Return the by hash fetching"""
        if 'download_by_hash' not in self.repoconfig:
            return self.default_download_by_hash

        return self.repoconfig['download_by_hash']

    @download_by_hash.setter
    def download_by_hash(self, newsetting: bool):
        """Set download_by_hash"""
        if isinstance(newsetting, bool):
            self.repoconfig['download_by_hash'] = newsetting

    @property
    def create_by_hash(self) -> bool:
        """Return the by hash fetching"""
        if 'create_by_hash' not in self.repoconfig:
            return self.default_create_by_hash

        return self.repoconfig['create_by_hash']

    @create_by_hash.setter
    def create_by_hash(self, newsetting: bool):
        """Set create_by_hash"""
        if isinstance(newsetting, bool):
            self.repoconfig['create_by_hash'] = newsetting

    @property
    def debian_installer(self) -> bool:
        """Return the debian_installer"""
        if 'debian_installer' not in self.repoconfig:
            return self.default_debian_installer

        return self.repoconfig['debian_installer']

    @debian_installer.setter
    def debian_installer(self, newsetting: bool):
        """Set debian_installer"""
        if isinstance(newsetting, bool):
            self.repoconfig['debian_installer'] = newsetting

    @property
    def cnf(self) -> bool:
        """Return the cnf"""
        if 'cnf' not in self.repoconfig:
            return self.default_cnf

        return self.repoconfig['cnf']

    @cnf.setter
    def cnf(self, newsetting: bool):
        """Set cnf"""
        if isinstance(newsetting, bool):
            self.repoconfig['cnf'] = newsetting

    @property
    def dep11(self) -> bool:
        """Return the dep11"""
        if 'cnf' not in self.repoconfig:
            return self.default_dep11

        return self.repoconfig['dep11']

    @dep11.setter
    def dep11(self, newsetting: bool):
        """Set dep11"""
        if isinstance(newsetting, bool):
            self.repoconfig['dep11'] = newsetting

    @property
    def dist_upgrader_all(self) -> bool:
        """Return the dist_upgrader_all"""
        if 'dist_upgrader_all' not in self.repoconfig:
            return self.default_dist_upgrader_all

        return self.repoconfig['dist_upgrader_all']

    @dist_upgrader_all.setter
    def dist_upgrader_all(self, newsetting: bool):
        """Set dist_upgrader_all"""
        if isinstance(newsetting, bool):
            self.repoconfig['dist_upgrader_all'] = newsetting

    @property
    def i18n(self) -> bool:
        """Return the i18n"""
        if 'i18n' not in self.repoconfig:
            return self.default_i18n

        return self.repoconfig['i18n']

    @i18n.setter
    def i18n(self, newsetting: bool):
        """Set i18n"""
        if isinstance(newsetting, bool):
            self.repoconfig['i18n'] = newsetting

    @property
    def languages(self) -> List[str]:
        """Return the wanted languages"""
        if 'languages' not in self.repoconfig:
            return self.default_languages

        return self.repoconfig['languages']

    @languages.setter
    def languages(self, newsetting: List[str]):
        """Set languages"""
        if isinstance(newsetting, list):
            self.repoconfig['languages'] = newsetting

    @property
    def installer(self) -> bool:
        """Return the installer"""
        if 'installer' not in self.repoconfig:
            return self.default_installer

        return self.repoconfig['installer']

    @installer.setter
    def installer(self, newsetting: bool):
        """Set installer"""
        if isinstance(newsetting, bool):
            self.repoconfig['installer'] = newsetting

    @property
    def uefi(self) -> bool:
        """Return the uefi"""
        if 'uefi' not in self.repoconfig:
            return self.default_uefi

        return self.repoconfig['uefi']

    @uefi.setter
    def uefi(self, newsetting: bool):
        """Set uefi"""
        if isinstance(newsetting, bool):
            self.repoconfig['uefi'] = newsetting

    @property
    def signed(self) -> bool:
        """Return the signed"""
        if 'signed' not in self.repoconfig:
            return self.default_signed

        return self.repoconfig['signed']

    @signed.setter
    def signed(self, newsetting: bool):
        """Set signed"""
        if isinstance(newsetting, bool):
            self.repoconfig['signed'] = newsetting

    @property
    def source(self) -> bool:
        """Return the source"""
        if 'source' not in self.repoconfig:
            return self.default_source

        return self.repoconfig['source']

    @source.setter
    def source(self, newsetting: bool):
        """Set uefi"""
        if not isinstance(newsetting, bool):
            self.repoconfig['source'] = newsetting

    @property
    def archive_root_url(self) -> str:
        urlhost = self.method + '://' + self.host
        url = urllib.parse.urljoin(urlhost, self.remote_directory)

        return url

    def configuration(self) -> Dict[str, Any]:
        config_dict: Dict[str, Any] = dict()
        config_dict['name'] = self.name
        config_dict['enabled'] = self.enabled
        config_dict['http_proxy'] = self.http_proxy
        config_dict['https_proxy'] = self.https_proxy
        config_dict['tls_verify'] = self.tls_verify
        config_dict['method'] = self.method
        config_dict['format'] = self.format
        config_dict['host'] = self.host
        config_dict['remote_directory'] = self.remote_directory
        config_dict['destination_directory'] = self.destination_directory
        config_dict['temp_destination_directory'] = self.temp_destination_directory
        config_dict['cache_directory'] = self.cache_directory
        config_dict['distributions'] = ', '.join(self.distributions.list)
        config_dict['components'] = ', '.join(self.components.list)
        config_dict['architectures'] = ', '.join(self.architectures.list)
        config_dict['download_packages'] = self.download_packages
        config_dict['download_diffs'] = self.download_diffs
        config_dict['download_by_hash'] = self.download_by_hash
        config_dict['create_by_hash'] = self.create_by_hash
        config_dict['debian_installer'] = self.debian_installer
        config_dict['cnf'] = self.cnf
        config_dict['dep11'] = self.dep11
        config_dict['dist_upgrader_all'] = self.dist_upgrader_all
        config_dict['i18n'] = self.i18n
        config_dict['languages'] = ', '.join(self.languages)
        config_dict['installer'] = self.installer
        config_dict['uefi'] = self.uefi
        config_dict['source'] = self.source
        config_dict['url'] = self.archive_root_url

        return config_dict

    def distribution_url(self, distribution: str) -> str:
        if self.format == 'deb':
            return os.path.join(self.archive_root_url, 'dists', distribution)
        if self.format == 'flat':
            return os.path.join(self.archive_root_url, distribution)

        return distribution

    def distribution_path(self, distribution: str) -> str:
        if self.format == 'deb':
            # Prepare for a temp directory when downloading
            # return os.path.join(self.temp_destination_directory, 'dists', distribution)
            return os.path.join(self.destination_directory, 'dists', distribution)
        if self.format == 'flat':
            # Prepare for a temp directory when downloading
            # return os.path.join(self.temp_destination_directory, distribution)
            return os.path.join(self.destination_directory, distribution)

        return distribution

    def file_match(self, filedata: Releasefileinfo) -> bool:
        if filedata.component not in self.components.list:
            if filedata.component != str():
                return False
        if filedata.architecture not in self.architectures.list:
            return False
        if filedata.relativepath.endswith('.diff/Index') and not self.download_diffs:
            return False

        if filedata.function == 'i18n':
            if not self.i18n:
                # print(filedata.filename, 'i18n')
                return False
            # If empty list, lets get all languages
            if self.languages == list():
                return True
            # Just choose requested languages
            if filedata.language not in self.languages:
                return False
        if filedata.function == 'debian-installer':
            if not self.debian_installer:
                return False
        if filedata.function == 'cnf':
            if not self.cnf:
                return False
        if filedata.function == 'dep11':
            if not self.dep11:
                return False
        if filedata.function == 'source':
            if not self.source:
                return False
        if re.match('installer-', filedata.function):
            if not self.installer:
                return False

        return True


class Mirrorconfig:
    def __init__(self, configurationfile: str = 'offlinemirror.ini'):
        # self.rawconfiguration: Dict[str, Dict[str, Union[str, bool, List[str]]]] = dict()
        self.rawconfiguration: MutableMapping[str, Any] = dict()
        self.reposconfig = dict()

        if not os.path.exists(configurationfile):
            raise FileNotFoundError(configurationfile)
        self.rawconfiguration = toml.load(configurationfile)

        http_proxy = str()
        https_proxy = str()
        tls_verify = True
        if 'global' in self.rawconfiguration.keys():
            if 'http_proxy' in self.rawconfiguration['global']:
                http_proxy = self.rawconfiguration['global']['http_proxy']
            if 'https_proxy' in self.rawconfiguration['global']:
                https_proxy = self.rawconfiguration['global']['https_proxy']
            if 'tls_verify' in self.rawconfiguration['global']:
                tls_verify = self.rawconfiguration['global']['tls_verify']
            # del self.rawconfiguration['global']

        for onerepo in self.rawconfiguration.keys():
            if onerepo != 'global':
                self.reposconfig[onerepo] = Repoobject(onerepo, self.rawconfiguration[onerepo],
                                                       http_proxy, https_proxy, tls_verify)

    @property
    def repos(self) -> List[str]:
        """Returns all repos in configuration file"""

        return list(self.reposconfig.keys())

    def repo(self, reponame: str) -> Repoobject:
        """Returns the repo configuration object"""
        if reponame not in self.repos:
            return Repoobject(reponame, dict())

        return self.reposconfig[reponame]

    @property
    def enabled_repos(self) -> List[str]:
        """Returns all enabled repos in configuration file"""
        currentrepos = list()
        for onerepo in self.repos:
            if self.reposconfig[onerepo].enabled:
                currentrepos.append(onerepo)

        return currentrepos

    def enable_only(self, repos: Union[List[str], str]) -> None:
        enable_repos = repos
        if isinstance(repos, str):
            enable_repos = [repos]
        for configrepo in self.repos:
            if configrepo in enable_repos:
                self.repo(configrepo).enable()
            else:
                self.repo(configrepo).disable()


def humanbytes(B: float) -> str:
    """Return the given bytes as a human friendly KB, MB, GB, or TB string.
    Found this on Stackoverflow, https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb
    """
    B = float(B)
    KB = float(1024)
    MB = float(KB ** 2)  # 1,048,576
    GB = float(KB ** 3)  # 1,073,741,824
    TB = float(KB ** 4)  # 1,099,511,627,776

    if B < KB:
        return '{0} {1}'.format(B, 'Bytes' if 0 == B > 1 else 'Byte')
    elif KB <= B < MB:
        return '{0:.2f} KB'.format(B / KB)
    elif MB <= B < GB:
        return '{0:.2f} MB'.format(B / MB)
    elif GB <= B < TB:
        return '{0:.2f} GB'.format(B / GB)
    elif TB <= B:
        return '{0:.2f} TB'.format(B / TB)

    return str(B) + ' B'


def readconfigurationfile(configpath: str) -> Tuple[Dict[str, Dict[str, Any]], bool]:
    """Reads the configuration file
       Input: Path to the configuration file
       Output: A dict
    """
    with open(configpath, 'rb') as configfile:
        configuration = json.load(configfile)
        return configuration, False

    return dict(), True


def create_directories(newpath: str) -> bool:
    onlypath = os.path.dirname(newpath)
    os.makedirs(onlypath, exist_ok=True)

    return True


def fetchfile(url: str, localpath: str,
              digesttype: str = 'sha256') -> Tuple[bool, str]:
    """Fetch a file
       Input: URL and path where to save the file, known hash of file
       Output: local path to the saved file
    """
    global proxies
    global verify
    if not create_directories(localpath):
        return False, "Error creating directories for %s" % localpath

    digest = hashlib.new(digesttype)
    try:
        request = requests.get(url, timeout=(5, 120), stream=True, proxies=proxies, verify=verify)
        if request.status_code != 200:
            return False, "HTTP error %d" % request.status_code

        # https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
        with open(localpath, 'wb') as handle:
            for block in request.iter_content(1024):
                handle.write(block)
                digest.update(block)
    except requests.exceptions.ReadTimeout as connerror:
        return False, f'[WARN] Read timeout fetching {url} {connerror}'
    except requests.exceptions.ConnectionError as connerror:
        return False, f'[WARN] Connection error while fetching {url} {connerror}'
    except requests.exceptions.RequestException as connerror:
        return False, f'[WARN] Request exception when fetching {url} {connerror}'

    return True, digest.hexdigest()


def match_lists(search: List[List[str]], data: List[str]) -> bool:
    """search is what to look for
       Input is [['amd64, 'i386'], ['main', 'restricted'], ['dep11']]
                ['dep11', 'amd64', 'main', 'restricted']
       AND between outer lists and OR between inner list
       data is the data to look in
    """
    and_ok = True
    for one_and in search:
        result = [element for element in one_and if element in data]
        if len(result) == 0:
            and_ok = False

    return and_ok


def download_release_file(repoconfig: Repoobject, distribution: str) -> str:
    """
        InRelease is preferred, Release is second
    """
    return_name = str()

    inreleasefileurl = os.path.join(repoconfig.distribution_url(distribution), 'InRelease')
    inrelease_file_path = os.path.join(repoconfig.distribution_path(distribution), 'InRelease')
    ok, checksum = fetchfile(inreleasefileurl, inrelease_file_path)
    if ok:
        return_name = inrelease_file_path

    releasefileurl = os.path.join(repoconfig.distribution_url(distribution), 'Release')
    release_file_path = os.path.join(repoconfig.distribution_path(distribution), 'Release')
    ok, checksum = fetchfile(releasefileurl, release_file_path)
    if not ok:
        return return_name
    else:
        if return_name == str():
            return release_file_path

    return return_name


def one_packages_file(prefix_path: str, p_files: List[Releasefileinfo]) -> Releasefileinfo:
    for one_packages in p_files:
        if os.path.exists(os.path.join(prefix_path, one_packages.relativepath)):
            return one_packages

    return Releasefileinfo()


def compile_release_files(repoconfig: Repoobject, relfile: Releasefile, distribution: str) -> List[Dict[str, str]]:
    filequeue: List[Dict[str, str]] = list()
    for onefile in relfile.files:
        saveinfo: Dict[str, str] = dict()
        filedata = relfile.file(onefile)
        if not repoconfig.file_match(filedata):
            continue
        if relfile.repoformat() == 'deb':
            saveinfo['filename'] = filedata.relativepath
            if relfile.acquirebyhash.value == 'yes' and repoconfig.download_by_hash:
                saveinfo['url'] = os.path.join(repoconfig.distribution_url(distribution),
                                               filedata.relativepath_byhash)
            else:
                saveinfo['url'] = os.path.join(repoconfig.distribution_url(distribution),
                                               filedata.relativepath)
            if repoconfig.create_by_hash:
                saveinfo['copyto'] = os.path.join(repoconfig.distribution_path(distribution),
                                                  filedata.relativepath_byhash)
            saveinfo['local'] = os.path.join(repoconfig.distribution_path(distribution),
                                             filedata.relativepath)
            filequeue.append(saveinfo)
        else:
            saveinfo['filename'] = filedata.filename
            saveinfo['url'] = os.path.join(repoconfig.distribution_url(distribution),
                                           filedata.relativepath)
            saveinfo['local'] = os.path.join(repoconfig.distribution_path(distribution),
                                             filedata.relativepath)
            filequeue.append(saveinfo)

    return filequeue


def list_packages_files(repoconfig: Repoobject, release: Releasefile, distribution: str) -> List[str]:
    package_files: List[str] = list()
    # Empty components list marks a flat format
    if repoconfig.components.list == list():
        package_file = one_packages_file(
            repoconfig.distribution_path(distribution),
            release.packages_files())
        package_files.append(os.path.join(repoconfig.distribution_path(distribution),
                                          package_file.relativepath))
    else:
        for component in repoconfig.components.list:
            for arch in repoconfig.architectures.list:
                if release.packages_files(component, arch) == list():
                    continue
                package_file = one_packages_file(
                    repoconfig.distribution_path(distribution),
                    release.packages_files(component, arch))
                if package_file.relativepath == str():
                    log.warning(f'[WARNING] Packages file for {distribution} {component} {arch} not found on filesystem. Perhaps "--trust 0"')
                    continue
                packages_file_path = os.path.join(repoconfig.distribution_path(distribution),
                                                  package_file.relativepath)
                package_files.append(packages_file_path)

    return package_files


def list_http_files(url: str) -> List[str]:
    extras_page = requests.get(url).text
    soup = BeautifulSoup(extras_page, 'html.parser')
    directory_list = list()
    for node in soup.find_all('a'):
        # print(f'Checking {node.get("href")}')
        if node.get('href').startswith('?'):
            continue
        if '/' in node.get('href'):
            continue
        directory_list.append(node.get('href'))

    return list(set(directory_list))


def list_http_dirs(url: str) -> List[str]:
    extras_page = requests.get(url).text
    soup = BeautifulSoup(extras_page, 'html.parser')
    directory_list = list()
    for node in soup.find_all('a'):
        # print(f'Checking {node.get("href")}')
        if node.get('href').endswith('/') and not node.get('href').startswith('/'):
            directory_list.append(node.get('href'))

    return list(set(directory_list))


def download_extra(extra: str, distribution: str, repoconfig: Repoobject, filecache: Cachedatabase) -> bool:
    """Download SHA256SUMS and the files inside"""
    for component in repoconfig.components.list:
        remote_url = os.path.join(repoconfig.distribution_url(distribution),
                                  component, extra)
        local_path = os.path.join(repoconfig.distribution_path(distribution),
                                  component, extra)
        http_dirs = list_http_dirs(remote_url)
        if len(http_dirs) > 0:
            log.info(f'[INFO] Downloading {distribution} -> {component} -> {extra}')
        for extra_directory in http_dirs:
            for extra_version in list_http_dirs(os.path.join(remote_url, extra_directory)):
                current_path = os.path.join(local_path, extra_directory, extra_version, 'SHA256SUMS')
                if filecache.expired(current_path):
                    filecache.remove(current_path)
                if not filecache.exists(current_path):
                    status, message = fetchfile(os.path.join(remote_url, extra_directory, extra_version, 'SHA256SUMS'),
                                                current_path)
                    if not status:
                        log.warning(f'[WARN] Failed to fetch {remote_url}: {message}')
                        continue
                # print(f'Fetch status {status} with message {message}')
                extra_files = Sha256sums()
                extra_files.load(current_path)
                for extra_file in extra_files.filenames:
                    save_path = os.path.join(local_path, extra_directory, extra_version, extra_file)
                    rem_url = os.path.join(remote_url, extra_directory, extra_version, extra_file)
                    if filecache.already_cached(save_path, extra_files.filename2hash(extra_file)):
                        # print(f'[CACHE] {extra_file} is cached and not downloaded')
                        continue
                    log.info(f'[INFO] Downloading {rem_url}')
                    status, message = fetchfile(rem_url, save_path)
                    # print(f'Fetched {rem_url}')
                    if status is False:
                        # print('[WARN] Fetch has failed')
                        continue

                    if message != extra_files.filename2hash(extra_file):
                        # print(f'[WARN] {extra_file} doesn\'t match {extra_files.filename2hash(extra_file)}')
                        continue

                    # print(f'[CACHE] Adding {save_path} to cache')
                    filecache.add(save_path, hashstring=message)

    return True


def download_by_directory_index(extra: str, distribution: str, repoconfig: Repoobject, filecache: Cachedatabase) -> bool:
    for component in repoconfig.components.list:
        remote_url = os.path.join(repoconfig.distribution_url(distribution),
                                  component, extra)
        local_path = os.path.join(repoconfig.distribution_path(distribution),
                                  component, extra)

        http_dirs = list_http_dirs(remote_url)
        if len(http_dirs) > 0:
            log.info(f'[INFO] Downloading {distribution} -> {component} -> {extra}')
        for extra_version in http_dirs:
            # print("extra_version", os.path.join(remote_url, extra_version))
            for extra_file in list_http_files(os.path.join(remote_url, extra_version)):
                # print("extra_file", extra_file)
                save_path = os.path.join(local_path, extra_version, extra_file)
                rem_url = os.path.join(remote_url, extra_version, extra_file)
                if filecache.already_cached(save_path):
                    # print(f'[CACHE] {save_path} already exists locally and is not downloaded')
                    continue

                # if filecache.expired(save_path):
                #     filecache.remove(save_path)
                # if filecache.exists(save_path):
                #     print(f'[CACHE] {save_path} already exists locally and is not downloaded')
                #     continue
                log.info(f'[FILE] Downloading {rem_url}')
                status, message = fetchfile(rem_url, save_path)
                # print(f'Fetched {rem_url}')
                if status is False:
                    # print('[WARN] Fetch has failed')
                    continue

                log.info(f'[CACHE] Adding {save_path} to cache')
                filecache.add(save_path, hashstring=message)

    return True


def download_debian_installer(distribution: str, release: Releasefile, repoconfig, filecache: Cachedatabase) -> bool:
    for one_file in release.files:
        filedata = release.file(one_file)
        if filedata.function != 'debian-installer':
            continue
        if not repoconfig.file_match(filedata):
            continue
        # local_path = os.path.join(repoconfig.temp_destination_directory,
        #                           'dists', distribution,
        #                           filedata.relativepath)
        local_path = os.path.join(repoconfig.distribution_path(distribution), filedata.relativepath)
        packages = Packagesfile()
        packages.load(local_path)
        for package in packages.packages:
            for package_version in packages.package(package):
                remote_url = os.path.join(repoconfig.archive_root_url, package_version.filename.value)
                # Prepare for temp directory when downloading
                # save_path = os.path.join(repoconfig.temp_destination_directory,
                #                          package_version.filename.value)
                save_path = os.path.join(repoconfig.destination_directory,
                                         package_version.filename.value)
                if filecache.already_cached(save_path, package_version.sha256.value):
                    continue
                result, checksum = fetchfile(remote_url, save_path)
                if result:
                    filecache.add(save_path, hashstring=checksum)
                    log.info(f'[FILE] Downloaded  {remote_url}')
                else:
                    log.error(f'[ERROR] {checksum} {remote_url}')

    return True


def download_source(distribution: str, release: Releasefile, repoconfig, filecache: Cachedatabase, arguments: argparse.Namespace) -> bool:
    log.info("Downloading sources")
    if release.source_files() == list():
        log.warning("[WARN] Sources index was not found")
        return False
    sources_file = release.source_files()[0]

    packages = Packagesfile()
    packages.url_base = repoconfig.archive_root_url
    # Prepare for temp directory when downloading
    # packages.directory_base = repoconfig.temp_destination_directory
    packages.directory_base = repoconfig.destination_directory
    packages.load(sources_file.path)
    for package in packages.packages:
        for package_version in packages.package(package):
            for one_source_file in package_version.files:
                local_file = os.path.join(package_version.path, str(one_source_file['filename']))
                remote_url = os.path.join(package_version.url, str(one_source_file['filename']))
                if filecache.negative_cached(local_file):
                    if arguments.verbose:
                        log.debug(f'[INFO] Negative cached {local_file} (404)')
                    continue
                if filecache.already_cached(os.path.join(package_version.path,
                                                         str(one_source_file['filename'])),
                                            str(one_source_file['sha256'])):
                    if arguments.verbose:
                        log.debug(f'[INFO] Already cached {one_source_file["filename"]}')
                    continue
                if arguments.verbose:
                    log.debug(f'[INFO] Downloading {one_source_file["filename"]}')
                result, checksum = fetchfile(remote_url, local_file)
                if result and checksum == one_source_file['sha256']:
                    log.info(f'[INFO] Downloaded {remote_url}')
                    if arguments.verbose:
                        log.debug(f'[INFO] Downloaded {remote_url} to {local_file}')
                    filecache.add(local_file, hashstring=checksum)
                else:
                    if arguments.verbose:
                        log.debug(f'[WARN] {remote_url} was not found, adding to negative cache.')
                    filecache.add(local_file, negative_cache=True)
                    # fatal_errors.append("Error: %s %s" % (checksum, remote_url))

    return True


def matching_regexp(onepackage: Packageinfo, repoconfig: Repoobject, arguments: argparse.Namespace) -> bool:
    if repoconfig.packagename_regexp != '' and re.search(repoconfig.packagename_regexp, onepackage.packagename.value):
        return True

    if repoconfig.filename_regexp != '' and re.search(repoconfig.filename_regexp, onepackage.filename.value):
        return True

    if repoconfig.packagename_regexp == '' and repoconfig.filename_regexp == '':
        return True

    if arguments.debug:
        log.debug(f'[INFO] Not downloading {onepackage.filename.value} (regexp)')

    return False


def update_repo(repoconfig: Repoobject, arguments: argparse.Namespace) -> bool:
    global proxies
    global verify
    if repoconfig.http_proxy != str():
        proxies['http_proxy'] = repoconfig.http_proxy
        proxies['http'] = repoconfig.http_proxy
    if repoconfig.https_proxy != str():
        proxies['https_proxy'] = repoconfig.https_proxy
        proxies['https'] = repoconfig.https_proxy
    verify = repoconfig.tls_verify

    fatal_errors: List[str] = list()
    filecache = Cachedatabase(os.path.join(repoconfig.cache_directory, repoconfig.reponame),
                              expire_days=arguments.trust)
    filecache.load()
    filecache.autosave_every = arguments.autosave

    for distribution in repoconfig.distributions.list:
        log.info(f'[INFO] Updating {repoconfig.name} -> {distribution}')
        current_release_file = download_release_file(repoconfig, distribution)
        release = Releasefile()
        release.url_base = repoconfig.distribution_url(distribution)
        release.directory_base = repoconfig.distribution_path(distribution)
        release.load(current_release_file)
        release_newqueue = compile_release_files(repoconfig, release, distribution)

        for queued_file in release_newqueue:
            filedata = release.file(queued_file['filename'])
            if filecache.already_cached(queued_file['local'], filedata.sha256):
                continue
            if filecache.negative_cached(queued_file["local"]):
                if arguments.verbose:
                    log.debug(f'[INFO] Negative cached {queued_file["local"]} (404)')
                release.remove(queued_file['filename'])
                continue
            if arguments.verbose:
                log.debug(f'[INFO] Downloading {queued_file["url"]}')
            if arguments.debug:
                log.debug(f'[INFO] Downloading {queued_file["url"]} to {queued_file["local"]}')
            result, checksum = fetchfile(queued_file['url'], queued_file['local'])
            if result:
                log.info(f'[INFO] Downloaded {queued_file["url"]}')
                if arguments.verbose:
                    log.debug(f'[INFO] Downloaded {queued_file["url"]} to {queued_file["local"]}')
                filecache.add(queued_file['local'], hashstring=checksum)
                if 'copyto' in queued_file:
                    if create_directories(queued_file['copyto']):
                        shutil.copyfile(queued_file['local'], queued_file['copyto'])
                        if arguments.debug:
                            log.debug(f'[INFO] Copied {queued_file["local"]} to {queued_file["copyto"]}')
                        filecache.add(queued_file["copyto"])

            else:
                if arguments.verbose:
                    log.debug(f'[WARN] {queued_file["url"]} was not found, adding to negative cache.')
                release.remove(queued_file['filename'])
                filecache.add(queued_file['local'], negative_cache=True)
                fatal_errors.append("Error: %s %s" % (checksum, queued_file['url']))

        if repoconfig.download_diffs:
            pass
        if repoconfig.download_packages:
            log.info(f'[INFO] Downloading all packages for {distribution}')
            packages_list = list_packages_files(repoconfig, release, distribution)
            for one_package_file in packages_list:
                if arguments.verbose:
                    log.debug(f'[INFO] Processing {one_package_file}')
                packages = Packagesfile()
                packages.load(one_package_file)
                if arguments.calculate:
                    bytes_to_download = 0
                    for package in packages.packages:
                        for package_version in packages.package(package):
                            # Prepare for temp directory when downloading
                            # local_file = os.path.join(repoconfig.temp_destination_directory,
                            #                           package_version.filename.value)
                            local_file = os.path.join(repoconfig.destination_directory,
                                                      package_version.filename.value)
                            if filecache.already_cached(local_file, package_version.sha256.value):
                                continue
                            bytes_to_download += int(package_version.size.value)
                    readable_bytes = humanbytes(bytes_to_download)
                    log.info(f'[INFO] Estimated download size for {repoconfig.name} -> {distribution}/{release} is {readable_bytes}')

                for package in packages.packages:
                    for package_version in packages.package(package):
                        if not matching_regexp(package_version, repoconfig, arguments):
                            if arguments.verbose:
                                log.debug(f'[INFO] Not downloading {package_version.packagename.value} (regexp match)')
                            continue
                        remote_url = os.path.join(repoconfig.archive_root_url, package_version.filename.value)
                        local_file = os.path.join(repoconfig.destination_directory,
                                                  package_version.filename.value)
                        if filecache.already_cached(local_file, package_version.sha256.value):
                            continue
                        if arguments.verbose:
                            log.debug(f'[FILE] Downloading {remote_url}')
                        result, checksum = fetchfile(remote_url, local_file)
                        if result:
                            filecache.add(local_file, hashstring=checksum)
                            log.info(f'[FILE] Downloaded {remote_url}')
                        else:
                            fatal_errors.append(f'[ERROR] {checksum} {remote_url}')
        if repoconfig.uefi:
            _ = download_extra('uefi', distribution, repoconfig, filecache)
        if repoconfig.signed:
            _ = download_extra('signed', distribution, repoconfig, filecache)
        if repoconfig.dist_upgrader_all:
            _ = download_by_directory_index('dist-upgrader-all', distribution, repoconfig, filecache)
        if repoconfig.debian_installer:
            _ = download_debian_installer(distribution, release, repoconfig, filecache)
        if repoconfig.source:
            _ = download_source(distribution, release, repoconfig, filecache, arguments)

    filecache.save()
    # TODO Move temp dists to active dists
    if arguments.errors:
        if len(fatal_errors) > 0:
            log.error('Errors when updating:')
            for oneerror in fatal_errors:
                print(oneerror)

    return True


def remove_expired_files(repoconfig: Repoobject, arguments: argparse.Namespace) -> int:
    filecache = Cachedatabase(os.path.join(repoconfig.cache_directory, repoconfig.reponame),
                              expire_days=arguments.trust)
    filecache.load()
    removed_objects: int = 0

    for one_cache in filecache.files():
        if filecache.expired(one_cache):
            filecache.remove(one_cache)
            if os.path.exists(one_cache):
                os.remove(one_cache)
            else:
                log.info(f'{one_cache} vanished, only removing from cache database')
            removed_objects += 1
            if arguments.verbose:
                log.info(f'[INFO] Removed expired entry {one_cache}')

    filecache.save()

    return removed_objects


def create_progressfile(repoconfig: Repoobject) -> bool:
    progress_file = os.path.join(repoconfig.destination_directory, f'.updating.{repoconfig.name}')
    with open(progress_file, 'w') as prfile:
        return True

    return False


def remove_progressfile(repoconfig: Repoobject) -> bool:
    progress_file = os.path.join(repoconfig.destination_directory, f'.updating.{repoconfig.name}')

    if os.path.exists(progress_file):
        try:
            os.remove(progress_file)
        except OSError:
            log.critical(f'[ERROR] Couldn\'t remove {progress_file}')
            return False

        return True

    return True


def update_repos(configuration: Mirrorconfig, arguments: argparse.Namespace):
    """Update all enabled repos"""
    for repo in configuration.enabled_repos:
        if arguments.progressfile:
            if create_progressfile(configuration.repo(repo)):
                if arguments.verbose:
                    log.debug(f'[INFO] Created a progress file for {repo}')
            else:
                log.warn('[WARN] Couldn\'t create a progress file')
        if update_repo(configuration.repo(repo), arguments):
            log.info("[INFO] Successfully updated %s" % repo)
        else:
            log.warning("[WARNING] Failed to update %s" % repo)
        if arguments.cleanup:
            removed_entries = remove_expired_files(configuration.repo(repo), arguments)
            if removed_entries > 0:
                log.info(f'[INFO] Removed {removed_entries} old expired files')
        if arguments.progressfile:
            if remove_progressfile(configuration.repo(repo)):
                if arguments.verbose:
                    log.debug(f'[INFO] Removing progressfile for {repo}')


def probe(url: str) -> str:
    probed: Dict[str, bool] = dict()
    probed['i18n'] = False
    probed['debian_installer'] = False
    probed['cnf'] = False
    probed['dep11'] = False
    languages: List[str] = list()
    dists_directory = os.path.join(url, 'Release')
    prul = urllib.parse.urlparse(dists_directory)
    # distributions = list_http_dirs(dists_directory)
    result, message = fetchfile(dists_directory, '/tmp/offlinemirror.probe')
    if not result:
        print(f'[ERROR] {message}')
        return str()
    release = Releasefile()
    release.load('/tmp/offlinemirror.probe')

    print(f'[{release.origin.value}]')
    print('enabled = true')
    print(f'method = "{prul.scheme}"')
    print(f'format = "{release.repoformat()}"')
    print(f'host = "{prul.hostname}"')
    path = prul.path
    to = path.find('/dists')
    print(f'remote_directory = "{path[0:to]}"')
    print('cache_directory = "."')
    print(f'destination_directory = ".{path[0:to]}"')
    print(f'distributions = {[os.path.basename(url)]}')
    print(f'components = {release.components.value}')
    print(f'architectures = {release.architectures.value}')
    print('download_packages = true')
    if release.acquirebyhash == 'yes':
        print('download_by_hash = true')
    else:
        print('download_by_hash = false')
    print('create_by_hash = true')
    for one_file in release.files:
        if release.file(one_file).function == 'i18n':
            probed['i18n'] = True
        if release.file(one_file).function == 'debian-installer':
            probed['debian_installer'] = True
        if release.file(one_file).function == 'cnf':
            probed['cnf'] = True
        if release.file(one_file).function == 'dep11':
            probed['dep11'] = True
        if release.file(one_file).language != 'unknown':
            languages.append(release.file(one_file).language)

    if probed['i18n']:
        print('i18n = true')
    print('languages =', list(set(languages)))
    if probed['debian_installer']:
        print('debian_installer = true')
    if probed['cnf']:
        print('cnf = true')
    if probed['dep11']:
        print('dep11 = true')

    return '# End'


def main():
    parser = argparse.ArgumentParser(description='Mirror DEB repositories')
    parser.add_argument('-c',
                        '--config',
                        dest='configfile',
                        action='store',
                        default='offlinemirror.ini',
                        help='Configuration file',
                        required=False)
    parser.add_argument('-v',
                        '--verbose',
                        dest='verbose',
                        default=False,
                        action='store_true',
                        help="Verbose output",
                        required=False)
    parser.add_argument('-d',
                        '--debug',
                        dest='debug',
                        default=False,
                        action='store_true',
                        help="Debug output",
                        required=False)
    parser.add_argument('--trust',
                        dest='trust',
                        default=30,
                        action='store',
                        metavar='days',
                        type=int,
                        help="Trust the cache for X (default 30) days, instead of calculate digest on every file",
                        required=False)
    parser.add_argument('--autosave',
                        dest='autosave',
                        default=500,
                        action='store',
                        type=int,
                        metavar='entries',
                        help="Autosave the cache for every X entry, when checking correctness of cache",
                        required=False)
    parser.add_argument('--errors',
                        dest='errors',
                        default=False,
                        action='store_true',
                        help="Show errors after",
                        required=False)
    parser.add_argument('--calculate',
                        dest='calculate',
                        default=False,
                        action='store_true',
                        help="Calculate and show packages download size before downloading",
                        required=False)
    parser.add_argument('--cleanup',
                        dest='cleanup',
                        default=False,
                        action='store_true',
                        help="Delete files from filesystem which is not referred to. File list from cache",
                        required=False)
    parser.add_argument('-l',
                        '--listrepos',
                        dest='listrepos',
                        default=False,
                        action='store',
                        nargs='*',
                        help='List configuration of all configured repos',
                        required=False)
    parser.add_argument('-u',
                        '--update',
                        dest='update',
                        default=False,
                        action='store',
                        nargs='*',
                        metavar='repos',
                        help='Update repos',
                        required=False)
    parser.add_argument('-p',
                        '--probe',
                        dest='probe',
                        default=False,
                        action='store',
                        help='Create a config by reading an URL',
                        required=False)
    parser.add_argument('-r',
                        '--progressfile',
                        dest='progressfile',
                        default=False,
                        action='store_true',
                        help="Create a file showing the repo is being updated",
                        required=False)

    args = parser.parse_args()

    if args.verbose or args.debug:
        log.setLevel(logging.DEBUG)

    if args.probe:
        new_config = probe(args.probe)
        print(new_config)
        sys.exit(0)

    configuration = Mirrorconfig(args.configfile)
    if args.listrepos is not False:
        allrepos = list()
        if args.listrepos == list():
            allrepos = configuration.repos
        else:
            allrepos = args.listrepos
        print("----------------------------------------------")
        for repo in allrepos:
            if repo not in configuration.repos:
                continue
            repoconfig = configuration.repo(repo)
            current_config = repoconfig.configuration()
            for conf in current_config:
                print('{:<22}: {:<20}'.format(conf, current_config[conf]))
            print("----------------------------------------------")

        sys.exit(0)

    if args.update is not False:
        if len(args.update) > 0:
            configuration.enable_only(args.update)

        update_repos(configuration, args)


if __name__ == "__main__":
    main()
