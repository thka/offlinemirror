#!/usr/bin/python3
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
from offlinemirror import (Cachedatabase, Componentsobject, Distributionsobject,
                           Architecturesobject, Mirrorconfig)


def test_cachedatabase():
    cache = Cachedatabase('cachefile.working')
    assert cache.expire_days == 30
    cache.load()
    assert cache.exists('/misc/Release') is True
    assert cache.exists('/misc/file_not_found') is False
    assert cache.files() == ['/misc/Packages.gz', '/misc/Release', '/misc/Package']
    # cache.add('tests/existing.file')
    # assert True is True
    #     86     def add(self, cacheobject: str, cachesize: int = -1,
    # 87             hashstring: str = '', negative_cache: bool = False) -> None:
    # 88         """
    # Add non existing file
    cache.add('/misc/nofile.txt')
    assert cache.files() == ['/misc/Packages.gz', '/misc/Release',
                             '/misc/Package', '/misc/nofile.txt']
    assert cache.digest('/misc/nofile.txt') == ''
    assert cache.size('/misc/nofile.txt') == -1

    # Remove file entry
    assert cache.remove('/misc/nofile.txt') is True
    assert cache.remove('/misc/file_not_found') is False

    cache.add('/misc/nofile.txt', cachesize=12345, hashstring='aaaaaaaaaaaaaaaaa9daab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d')
    assert cache.digest('/misc/nofile.txt') == 'aaaaaaaaaaaaaaaaa9daab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d'
    cache.setdigest('/misc/nofile.txt', 'bbbbbbbbbbbbbbbbbbdaab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d')
    assert cache.digest('/misc/nofile.txt') == 'bbbbbbbbbbbbbbbbbbdaab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d'
    assert cache.size('/misc/nofile.txt') == 12345
    cache.remove('/misc/nofile.txt')

    # Add existing file
    cache.add('./newfile.txt')
    assert cache.size('./newfile.txt') == 12
    assert cache.digest('./newfile.txt') == '1d40396d9c3fa723a9daab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d'
    assert cache.digesttype('1d40396d9c3fa723a9daab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d') == 'sha256'

    # days_old
    assert cache.days_old('/misc/Release') > 0
    assert cache.days_old('/misc/file_not_found') == 0

    # Check lastseen
    assert cache.lastseen('/misc/Release') == '2022-09-27 18:30:04'
    new_lastseen = cache.touch('/misc/Release')
    assert cache.cache_entry('/misc/Release')['lastseen'] == new_lastseen

    assert cache.comparedigest('./newfile.txt', '1d40396d9c3fa723a9daab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d') is True
    assert cache.comparedigest('./newfile.txt', 'aaa0396d9c3fa723a9daab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d') is False

    # Check expiry
    assert cache.expired('/misc/packages.gz') is True

    # Already cached
    assert cache.already_cached('./newfile.txt', '1d40396d9c3fa723a9daab2a6175de7479dae6ef2656ea6cb5b83fa2e133e19d') is True
    assert cache.already_cached('./not_in_cache.txt', 'aaaaaaaaaf82d25f0a5b33ea5d280b72499565ad6a804dea9c4b9bdeca2d4a39') is False
    assert cache.exists('./not_in_cache.txt') is False
    assert cache.already_cached('./not_in_cache.txt', 'c7c7c9981f82d25f0a5b33ea5d280b72499565ad6a804dea9c4b9bdeca2d4a39') is True
    assert cache.exists('./not_in_cache.txt') is True

    # Negative cached
    assert cache.negative_cached('./newfile.txt') is False
    assert cache.negative_cached('./file_not_found.txt') is False
    assert cache.negative_cached('/misc/Package') is True


def test_componentsobject():
    component_object = Componentsobject(['main', 'contrib'])
    assert component_object.list == ['main', 'contrib']
    component_object.list = 'main'
    assert component_object.list == ['main']
    component_object.list = ['main', 'contrib']
    assert component_object.list == ['main', 'contrib']
    component_object.add('restricted')
    assert component_object.list == ['main', 'contrib', 'restricted']
    component_object.remove('restricted')
    assert component_object.list == ['main', 'contrib']
    assert component_object.path('main') == 'main'


def test_distributionsobject():
    distribution_object = Distributionsobject(['bionic', 'focal'])
    assert distribution_object.list == ['bionic', 'focal']
    distribution_object.list = 'bionic'
    assert distribution_object.list == ['bionic']
    distribution_object.list = ['bionic', 'focal']
    assert distribution_object.list == ['bionic', 'focal']
    distribution_object.add('jammy')
    assert distribution_object.list == ['bionic', 'focal', 'jammy']
    distribution_object.remove('jammy')
    assert distribution_object.list == ['bionic', 'focal']
    assert distribution_object.path('bionic') == 'dists/bionic'


def test_architectureobject():
    architecture_object = Architecturesobject(['amd64', 'i386'])
    assert architecture_object.list == ['amd64', 'i386']
    architecture_object.list = 'amd64'
    assert architecture_object.list == ['amd64']
    architecture_object.list = ['amd64', 'i386']
    assert architecture_object.list == ['amd64', 'i386']
    architecture_object.add('arm64')
    assert architecture_object.list == ['amd64', 'i386', 'arm64']
    architecture_object.remove('arm64')
    assert architecture_object.list == ['amd64', 'i386']


def test_mirrorconfig():
    config = Mirrorconfig('test.ini')
    assert config.repos == ['Ubuntu', 'Debian', 'Jenkins']
    assert config.enabled_repos == ['Ubuntu']
    config.enable_only('Debian')
    assert config.enabled_repos == ['Debian']


def test_repoconfig():
    config = Mirrorconfig('test.ini')
    ubuntu = config.repo('Ubuntu')
    assert ubuntu.name == 'Ubuntu'
    ubuntu.disable()
    assert ubuntu.enabled is False
    ubuntu.enable()
    assert ubuntu.enabled is True
    assert ubuntu.method == 'http'
    ubuntu.method = 'https'
    assert ubuntu.method == 'https'
    assert ubuntu.format == 'deb'
    ubuntu.format = 'flat'
    assert ubuntu.format == 'flat'
    ubuntu.format = 'deb'
    assert ubuntu.host == 'se.archive.ubuntu.com'
    assert ubuntu.remote_directory == '/ubuntu'
    assert ubuntu.destination_directory == '/misc/offlinemirror/ubuntu'
    # assert ubuntu.temp_destination_directory == '/misc/offlinemirror/ubuntu/.offlinemirror.Ubuntu'
    assert ubuntu.cache_directory == '/misc/offlinemirror/cache'
    assert ubuntu.distributions.list == ['focal']
    assert ubuntu.components.list == ['main', 'restricted', 'universe', 'multiverse']
    assert ubuntu.architectures.list == ['amd64', 'all']
    assert ubuntu.download_packages is False
    assert ubuntu.download_by_hash is True
    assert ubuntu.create_by_hash is True
    assert ubuntu.debian_installer is True
    assert ubuntu.cnf is False
    assert ubuntu.dep11 is False
    assert ubuntu.dist_upgrader_all is False
    assert ubuntu.i18n is False
    assert ubuntu.languages == ['sv']
    assert ubuntu.installer is False
    assert ubuntu.uefi is True
    assert ubuntu.signed is True
    assert ubuntu.source is False
    assert ubuntu.archive_root_url == 'https://se.archive.ubuntu.com/ubuntu'
    assert ubuntu.distribution_url('focal') == 'https://se.archive.ubuntu.com/ubuntu/dists/focal'
    # assert ubuntu.distribution_path('focal') == '/misc/offlinemirror/ubuntu/.offlinemirror.Ubuntu/dists/focal'
    assert ubuntu.distribution_path('focal') == '/misc/offlinemirror/ubuntu/dists/focal'
    assert ubuntu.http_proxy == str()
    assert ubuntu.https_proxy == 'http://localhost:8080'
    assert ubuntu.autosave == 500
    assert ubuntu.packagename_regexp == ''
    assert ubuntu.filename_regexp == ''
